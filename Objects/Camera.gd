extends SpringArm

const MIN_ANGLE_X = -55;
const MAX_ANGLE_X = 0;
const CAMERA_ROTATION_SPEED = 1.0;
const MIN_ARM_DISTANCE = 10;
const MAX_ARM_DISTANCE = 20;
const CAMERA_ZOOM_SPEED = 1;


func _ready():
	pass 

func _process(delta):
	
	# Use actions to determine if we are rotating camera
	# Not sure if more efficient in input method but here we are
	var rotation_y = 0.0;
	if Input.is_action_pressed("camera_rotate_left"):
		rotation_y += -CAMERA_ROTATION_SPEED;
	if Input.is_action_pressed("camera_rotate_right"):
		rotation_y += CAMERA_ROTATION_SPEED;
		
	var rotation_x = 0.0;
	if Input.is_action_pressed("camera_rotate_up"):
		rotation_x += -CAMERA_ROTATION_SPEED;
	if Input.is_action_pressed("camera_rotate_down"):
		rotation_x += CAMERA_ROTATION_SPEED;
		
	# Ensure rotations are clamped within their values so we don't end up with weird rotations
	var current_rotation = rotation_degrees;
	current_rotation.x += rotation_x;
	current_rotation.x = clamp(current_rotation.x, MIN_ANGLE_X, MAX_ANGLE_X);
	current_rotation.y += rotation_y;
	current_rotation.z = 0;
	rotation_degrees = current_rotation;
	
func _input(event):
	
	# Can't use action pressed for mouse wheel scroll so use input method instead
	if event is InputEventMouseButton:
		if event.is_pressed() and not event.is_echo():
			# Confirmed input from mouse so check if mouse wheel pressed
			var current_spring_length = spring_length;
			if event.button_index == BUTTON_WHEEL_DOWN:
				current_spring_length += CAMERA_ZOOM_SPEED;
			if event.button_index == BUTTON_WHEEL_UP:
				current_spring_length += -CAMERA_ZOOM_SPEED;
		
			# Ensure the zoom length is clamped
			spring_length = clamp(current_spring_length, MIN_ARM_DISTANCE, MAX_ARM_DISTANCE);
		